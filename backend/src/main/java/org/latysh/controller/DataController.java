package org.latysh.controller;

import org.latysh.domain.Device;
import org.latysh.services.DeviceService;
import org.latysh.services.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("history")
public class DataController {
    private final HistoryService historyService;
    private final DeviceService deviceService;

    @Autowired
    public DataController(HistoryService historyService, DeviceService deviceService) {
        this.historyService = historyService;
        this.deviceService = deviceService;
    }

    @GetMapping
    public Map<Long, Double> getDeviceHistory(
            @RequestParam(value = "deviceId") String deviceId,
            @RequestParam(value = "deviceSerial") String deviceSerial,
            @RequestParam(value = "from") long from,
            @RequestParam(value = "to") long to,
            @RequestParam(value = "detail", required = false, defaultValue = "0") Integer detail) {
        if (from > to) {
            long swap = from;
            from = to;
            to = swap;
        }

        return historyService.prepareAndFillDataFromDeviceHistory(deviceId, deviceSerial,
                from, to, detail);
    }

    @PostMapping
    public void create(@RequestBody Device device) {
        final Device[] entity = {null};
        deviceService.getAll().forEach( device1 -> {
            if(device1.getSerial().equals(device.getSerial())) {
                entity[0] = device1;
            }
        });
        historyService.saveOrUpdateChangeValueDataForDevice(entity[0], device.getValue());
    }
}
