package org.latysh.controller;

import com.fasterxml.jackson.annotation.JsonView;
import org.latysh.domain.Device;
import org.latysh.domain.Views;
import org.latysh.dto.EventType;
import org.latysh.dto.ObjectType;
import org.latysh.services.DeviceService;
import org.latysh.services.UserService;
import org.latysh.util.WsSender;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.function.BiConsumer;

@RestController
@RequestMapping("device")
public class DeviceApiController {
    private final DeviceService deviceService;
    private final UserService userService;
    private final BiConsumer<EventType, Device> wsSender;

    @Autowired
    public DeviceApiController(DeviceService deviceService, UserService userService, WsSender wsSender) {
        this.deviceService = deviceService;
        this.userService = userService;
        this.wsSender = wsSender.getSender(ObjectType.DEVICE, Views.IdName.class);
    }

    @GetMapping
    @JsonView(Views.IdName.class)
    public List<Device> getAllDevices() {
        return userService.getAllDevices();
    }

    @GetMapping("{serial}")
    @JsonView(Views.FullDevice.class)
    public Device getOne(@PathVariable("serial") String serial) {
        return userService.addDeviceForUser(serial);
    }

    @PostMapping
    public Device create(@RequestBody Device device) {
        device.setCreationDate(LocalDateTime.now());
        device.setId(UUID.randomUUID().toString());
        Device savedDevice = deviceService.save(device);
        wsSender.accept(EventType.CREATE, savedDevice);

        return savedDevice;
    }

    @PutMapping("{id}")
    public Device update(@PathVariable("id") Device deviceFromDb,
                         @RequestBody Device device) {
        BeanUtils.copyProperties(device, deviceFromDb, "id");
        Device updatedDevice = deviceService.save(deviceFromDb);
        wsSender.accept(EventType.UPDATE, updatedDevice);
        return updatedDevice;
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") String deviceId) {
        Device device = deviceService.getById(deviceId);
        deviceService.remove(device);
        wsSender.accept(EventType.REMOVE, device);
    }
}
