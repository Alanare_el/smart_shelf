package org.latysh.dto;

/**
 * Класс для хранения данных с устройства
 */
public class Time {

    /**
     * Время получения данных
     */
    private Long time;
    /**
     * Данные с устройства
     */
    private Double value;

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

}
