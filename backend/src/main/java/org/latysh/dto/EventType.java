package org.latysh.dto;

public enum EventType {
    CREATE,
    UPDATE,
    REMOVE
}
