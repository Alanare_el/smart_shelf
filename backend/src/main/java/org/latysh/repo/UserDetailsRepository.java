package org.latysh.repo;

import org.latysh.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class UserDetailsRepository {
    private static final String COLLECTION_NAME = "users";
    private static final Logger logger = LoggerFactory.getLogger(User.class);

    private MongoTemplate mongoTemplate;

    @Autowired
    public UserDetailsRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }


    public User save(User entity) {
        logger.debug("save <- entity: {}", entity);

        entity = mongoTemplate.save(entity);

        logger.debug("save -> done: {}", entity);

        return entity;
    }

    public Optional<User> findByName(String userName) {
        Query q = new Query(Criteria.where("name").is(userName));
        return mongoTemplate.find(q, User.class, COLLECTION_NAME).stream().findFirst();
    }


   public User findById(String id) {
       logger.debug("findById <- id = '{}'", id);

       User entity = mongoTemplate.findById(id, User.class);

       logger.debug("findByName -> {}", entity);
       return entity;
   }

}
