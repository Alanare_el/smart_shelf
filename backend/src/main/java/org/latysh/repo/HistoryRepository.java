package org.latysh.repo;

import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.latysh.domain.History;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.sql.Time;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Repository
public class HistoryRepository {

    private static final String COLLECTION_NAME = "history";
    private static final Logger logger = LoggerFactory.getLogger(HistoryRepository.class);

    private static final String _ID = "_id";
    private static final String MONGO_DOT = ".";

    public static final String FIELD_DAY = "day";
    public static final String FIELD_TIMELIST = "timeList";
    public static final String FIELD_HOURLIST = "hoursList";
    public static final String FIELD_MINUTELIST = "minutesList";

    private MongoTemplate mongoTemplate;

    @Autowired
    public HistoryRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public UpdateResult saveOrUpdate(History dto) {
        Document dbDoc = new Document();
        mongoTemplate.getConverter().write(dto, dbDoc);
        Document find = new Document("_id", dto.getId());
        dbDoc.remove("_id");
        Document update = new Document("$set", dbDoc);
        UpdateOptions options = new UpdateOptions().upsert(true);

        return mongoTemplate.getDb().getCollection(COLLECTION_NAME).updateOne(find, update, options);
    }

    public Optional<History> findById(String id) {
        Query idQuery = getIdQuery(id);
        return mongoTemplate.find(idQuery, History.class, COLLECTION_NAME).stream().findFirst();
    }

    public History convertToDTO(Object obj) {
        Document dbDoc = new Document();
        mongoTemplate.getConverter().write(obj, dbDoc);
        dbDoc.remove("_class");
        return mongoTemplate.getConverter().read(History.class, dbDoc);
    }

    public void updateHistoryData(String id, Time day, long noMin, Time hours, long noSec, Time minutes, Time seconds) {
        org.bson.Document filter = new org.bson.Document(_ID, id);

        org.bson.Document dayDoc = new org.bson.Document();
        mongoTemplate.getConverter().write(day, dayDoc);
        dayDoc.remove("_class");

        org.bson.Document hoursDoc = new org.bson.Document();
        mongoTemplate.getConverter().write(hours, hoursDoc);
        hoursDoc.remove("_class");

        org.bson.Document minutesDoc = new org.bson.Document();
        mongoTemplate.getConverter().write(minutes, minutesDoc);
        minutesDoc.remove("_class");

        org.bson.Document secondsDoc = new org.bson.Document();
        mongoTemplate.getConverter().write(seconds, secondsDoc);
        secondsDoc.remove("_class");

        org.bson.Document root = new org.bson.Document();
        org.bson.Document setData = new org.bson.Document();
        setData.put(FIELD_MINUTELIST + MONGO_DOT + noSec, minutesDoc);
        setData.put(FIELD_HOURLIST + MONGO_DOT + noMin, hoursDoc);
        setData.put(FIELD_DAY, dayDoc);

        root.put("$push", new org.bson.Document(FIELD_TIMELIST, secondsDoc));
        root.put("$set", setData);

        mongoTemplate.getDb().getCollection(COLLECTION_NAME).updateOne(filter, root);
    }

    public Optional<History> getCurrentValueByHistoryId(String historyId) {
        Query q = new Query();
        q.addCriteria(Criteria.where("historyId").is(historyId));
        q.with(new Sort(Sort.Direction.DESC, "_id"));
        q.limit(1);
        q.fields()
                .exclude("timeList")
                .exclude("hoursList")
                .exclude("minutesList");

        return Optional.ofNullable(mongoTemplate.findOne(q, History.class));

    }

    public List<History> getListHistoryByTimeHash(List<String> time) {
        Query q = new Query();
        q.addCriteria(Criteria.where("_id").in(time));

        return mongoTemplate.find(q, History.class);
    }

    public List<History> getHistoryIdsByField(String key, String value) {
        Query query = Query.query(Criteria.where(key).is(value));
        query.fields().include(_ID);

        return mongoTemplate.find(Query.query(Criteria.where(key).is(value)), History.class);
    }

    public List<History> getHistoryForDevicesByDeviceSerial(String deviceId,
                                                            String deviceSerial,
                                                            long from, long to, int detail) {
        String fromStr = Instant.ofEpochSecond(from).toString().substring(0, 10);
        String toStr = Instant.ofEpochSecond(to + 86400).toString().substring(0, 10);

        Query q = new Query();
        q.addCriteria(Criteria.where("deviceId").is(deviceId));
        q.addCriteria(Criteria.where("deviceSerial").is(deviceSerial));
        q.addCriteria(Criteria.where("_id").gt(fromStr).lt(toStr));
        q.with(new Sort(Sort.Direction.DESC, "_id"));
        excludeFieldsByDetail(q, detail);

        return mongoTemplate.find(q, History.class);
    }

    /**
     * Add exclude fields to query for optimise mapping to pojo.
     *
     * @param q      - query
     * @param detail - type detail data
     */
    private static void excludeFieldsByDetail(Query q, int detail) {
        switch (detail) {
            case 4: {
                q.fields()
                        .exclude("timeList")
                        .exclude("hoursList")
                        .exclude("minutesList");
                break;
            }
            case 3: {
                q.fields()
                        .exclude("day")
                        .exclude("timeList")
                        .exclude("minutesList");
                break;
            }
            case 2: {
                q.fields()
                        .exclude("day")
                        .exclude("timeList")
                        .exclude("hoursList");
                break;
            }
            default: {
                q.fields()
                        .exclude("day")
                        .exclude("hoursList")
                        .exclude("minutesList");
                break;
            }
        }
    }

    private Query getIdQuery(String id) {
        return new Query(Criteria.where("_id").is(id));
    }
}
