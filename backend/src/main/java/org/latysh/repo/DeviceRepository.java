package org.latysh.repo;

import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.latysh.domain.Device;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class DeviceRepository {
    private static final String COLLECTION_NAME = "devices";
    private static final Logger logger = LoggerFactory.getLogger(DeviceRepository.class);

    private MongoTemplate mongoTemplate;

    @Autowired
    public DeviceRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public Device save(Device entity) {
        logger.debug("save <- device: '{}'", entity);

        saveOrUpdate(entity);

        logger.debug("save -> {}", entity);
        return entity;
    }

    public Device findById(String id) {
        logger.debug("findById <- name = '{}'", id);

        Device entity = mongoTemplate.findById(id, Device.class);

        logger.debug("findById -> {}", entity);
        return entity;
    }

    public List<Device> findAll() {
        List<Device> entities = mongoTemplate.findAll(Device.class);
        logger.debug("findAll -> {}", entities);
        return entities;
    }

    public void remove(Device device) {
        mongoTemplate.remove(device);
        logger.debug("remove -> {}", device);
    }

    public Optional<Device> findBySerial(String serial) {
        Query q = new Query(Criteria.where("serial").is(serial));
        return mongoTemplate.find(q, Device.class, COLLECTION_NAME).stream().findFirst();
    }

    private UpdateResult saveOrUpdate(Device entity) {
        Document find = new Document("_id", entity.getId());

        Document target = new Document();

        mongoTemplate.getConverter().write(entity, target);
        target.put("serial", entity.getSerial());
        target.put("value", entity.getValue());
        target.put("localDateTime", entity.getLastActionTime());

        Document update = new Document("$set", target);

        UpdateOptions options = new UpdateOptions().upsert(true);

        return mongoTemplate.getDb().getCollection(Device.COLLECTION_NAME).updateOne(find, update, options);
    }

}