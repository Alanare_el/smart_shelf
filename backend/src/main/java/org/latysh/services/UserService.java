package org.latysh.services;

import org.codehaus.jackson.map.ObjectMapper;
import org.latysh.domain.Device;
import org.latysh.domain.User;
import org.latysh.repo.DeviceRepository;
import org.latysh.repo.UserDetailsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;


@Service
public class UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    private final DeviceRepository deviceRepository;
    private final UserDetailsRepository userDetailsRepository;

    @Autowired
    public UserService(DeviceRepository deviceRepository, UserDetailsRepository userDetailsRepository) {
        this.deviceRepository = deviceRepository;
        this.userDetailsRepository = userDetailsRepository;
    }

    public Device addDeviceForUser(String serial) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object currentPrincipal = authentication.getPrincipal();
        User user = (User) currentPrincipal;
        Optional<Device> deviceOpt = deviceRepository.findBySerial(serial);
        final Device[] deviceObj = {null};
        deviceOpt.ifPresent( device -> {
            deviceObj[0] = device;
            user.getDevices().add(device);
            userDetailsRepository.save(user);
        });
        return deviceObj[0];
    }

    public List<Device> getAllDevices() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object currentPrincipal = authentication.getPrincipal();
        User user = (User) currentPrincipal;
        return user.getDevices();
    }

    public User save(User user) {
        return userDetailsRepository.save(user);
    }

    public User getById(String id) {
        return userDetailsRepository.findById(id);
    }

    private String getUserNameFromJson(String json) {
        String name = "";
        try {
            User user = new ObjectMapper().readValue(json, User.class);
            name = user.getName();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        return name;
    }
}
