package org.latysh.services;

import org.latysh.domain.Device;
import org.latysh.domain.History;
import org.latysh.dto.Time;
import org.latysh.repo.DeviceRepository;
import org.latysh.repo.HistoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class HistoryService {

    private static final Logger logger = LoggerFactory.getLogger(HistoryService.class);

    @Autowired
    private HistoryRepository repository;

    @Autowired
    private DeviceRepository deviceRepository;

    /**
     * @param deviceId     -  device id
     * @param deviceSerial - device serial number
     * @param from         - ins time from
     * @param to           - inst time to
     * @param detail       - detail report {1- all time, 2- by minute,3 - by hours, 4- by days }
     * @return Map results (epochTime, value - by detail )
     */
    public Map<Long, Double> prepareAndFillDataFromDeviceHistory(String deviceId, String deviceSerial,
                                                                 long from, long to, int detail) {
        if (detail == 4) {
            return prepareAndFillDataFromPropsHistoryForGraphByDay(deviceId, deviceSerial, from, to);
        }
        List<History> historyList = repository.getHistoryForDevicesByDeviceSerial(deviceId,
                deviceSerial, from, to, detail);
        Map<Long, Double> aggregateData = getAggregateData(detail, historyList, from, to);
        excludeDoubleValuesInTimeSection(aggregateData, detail);
        return aggregateData;
    }

    public void saveOrUpdateChangeValueDataForDevice(Device device, String newData) {
        History history = fillDtoHistoryByDeviceData(device);

        Optional<History> updatedHistory = repository.findById(history.getId());
        updatedHistory.ifPresent(updatedHistoryD -> {
            updatedHistory.get().setCurrentValue(newData);
        });
        updatedHistory.ifPresent(updatedHistoryDto -> {
            device.setValue(newData);
            fillAggregateData(Instant.now(), updatedHistoryDto, updatedHistoryDto.getCurrentValue());
            deviceRepository.save(device);
            repository.saveOrUpdate(updatedHistoryDto);
        });
    }

    private void fillAggregateData(Instant now, History history, String value) {
        LocalDateTime localDateTime = LocalDateTime
                .ofEpochSecond(now.getEpochSecond(), 0, ZoneOffset.UTC);
        long noSec = localDateTime.truncatedTo(ChronoUnit.MINUTES).toEpochSecond(ZoneOffset.UTC);
        long noMin = localDateTime.truncatedTo(ChronoUnit.HOURS).toEpochSecond(ZoneOffset.UTC);

        fillMinutesList(history, value, noSec);
        fillHourList(history, value, noMin);
        Time newTime = fillTimeList(now, history, value);
        history.setDay(newTime);
    }

    private void fillMinutesList(History history, String value, long noSec) {
        Time newTime = calcMinutesList(history, value, noSec);
        if (history.getMinutesList() == null) {
            HashMap<Long, Time> minutesMap = new HashMap<>();
            minutesMap.put(noSec, newTime);
            history.setMinutesList(minutesMap);
        } else {
            history.getMinutesList().put(noSec, newTime);
        }

    }

    private History fillDtoHistoryByDeviceData(Device device) {
        //fill new values for record
        HashMap<String, Object> historyMap = new HashMap<>();
        historyMap.put("deviceId", device.getId());
        historyMap.put("deviceSerial", device.getSerial());

        History history = repository.convertToDTO(historyMap);
        //Generate id and hash
        history.setId();
        history.setHash(String.valueOf(history.hashCode()));
        repository.saveOrUpdate(history);
        return history;
    }

    private void fillHourList(History history, String value, long noMin) {
        Time newTime = calcHoursList(history, value, noMin);
        if (history.getHoursList() == null) {
            LinkedHashMap<Long, Time> hourMap = new LinkedHashMap<>();
            hourMap.put(noMin, newTime);
            history.setHoursList(hourMap);
        } else {
            history.getHoursList().put(noMin, newTime);
        }
    }

    private Time calcMinutesList(History historyDTO, @NotNull String value, long noSec) {
        Double newValue = Double.valueOf(value);
        Time newTime = new Time();
        if (historyDTO != null && !CollectionUtils.isEmpty(historyDTO.getMinutesList())) {
            Long timeLastAll = historyDTO.getTimeList().getLast().getTime();
            long lastTimeIns = LocalDateTime.
                    ofEpochSecond(timeLastAll, 0, ZoneOffset.UTC).
                    truncatedTo(ChronoUnit.MINUTES).toEpochSecond(ZoneOffset.UTC);

            Time timeMinutesCollision = historyDTO.getMinutesList().get(noSec);
            if (timeMinutesCollision == null) {
                newTime.setValue(newValue);
            } else {
                historyDTO.getMinutesList().remove(lastTimeIns);
                newTime.setValue(newValue);
            }

        } else {
            newTime.setValue(newValue);
        }

        return newTime;
    }

    private Time calcHoursList(History historyDTO, @NotNull String value, long noMin) {
        Double newValue = Double.valueOf(value);
        Time newTime = new Time();
        if (historyDTO != null && !CollectionUtils.isEmpty(historyDTO.getHoursList())) {
            Long timeLastAll = historyDTO.getTimeList().getLast().getTime();
            long lastTimeIns = LocalDateTime.
                    ofEpochSecond(timeLastAll, 0, ZoneOffset.UTC).
                    truncatedTo(ChronoUnit.HOURS).toEpochSecond(ZoneOffset.UTC);

            Time timeHourCollision = historyDTO.getHoursList().get(noMin);
            if (timeHourCollision == null) {

                newTime.setValue(newValue);
            } else {
                historyDTO.getHoursList().remove(lastTimeIns);
                newTime.setValue(newValue);
            }
        } else {

            newTime.setValue(newValue);
        }

        return newTime;
    }

    private Time fillTimeList(Instant now, History history, String value) {
        Time newTime = calcTimeList(history, value, now);
        if (history.getTimeList() == null) {
            LinkedList<Time> newTimeList = new LinkedList<>();
            newTimeList.add(newTime);
            history.setTimeList(newTimeList);
            return newTime;
        } else {
            history.getTimeList().add(newTime);
            Time dayTime = new Time();
            dayTime.setTime(now.getEpochSecond());
            dayTime.setValue(newTime.getValue());
            return dayTime;
        }
    }

    private Time calcTimeList(History historyDTO, @NotNull String value, Instant now) {
        Double newValue = Double.valueOf(value);
        Time newTime = new Time();
        if (historyDTO != null && !CollectionUtils.isEmpty(historyDTO.getTimeList())) {
            newTime.setValue(newValue);
            newTime.setTime(now.getEpochSecond());
        } else {
            newTime.setValue(newValue);
            newTime.setTime(now.getEpochSecond());
        }

        return newTime;
    }

    /**
     * Чтобы точно выдать историю значений с дневной детализацией в нужном для пользователя часовом поясе,
     * собираем все данные с часовой детализацией, и отсеиваем неактульные данные относительно суточного времени,
     * переданного пользователем.
     * Например у пользователя день начинается в 17:00 по UTC, значит последняя запись в документе до 17:00 это один
     * день, а после 17:00 - уже следующий день.
     */
    private Map<Long, Double> prepareAndFillDataFromPropsHistoryForGraphByDay(String deviceId, String deviceSerial,
                                                                             long from, long to) {
        List<History> historyList = repository.getHistoryForDevicesByDeviceSerial(deviceId,
                deviceSerial, from, to, 3);
        Map<Long, Double> aggregateData = getAggregateData(3, historyList, from, to);

        Map<Long, Long> days = new HashMap<>();
        if (!CollectionUtils.isEmpty(aggregateData)) {
            List<Long> hours = aggregateData.keySet()
                    .stream()
                    .sorted(Long::compareTo)
                    .collect(Collectors.toList());

            for (Long hour : hours) {
                LocalDateTime targetTime = LocalDateTime.ofEpochSecond(hour, 0, ZoneOffset.UTC);
                long dayRounded = targetTime.truncatedTo(ChronoUnit.DAYS).toEpochSecond(ZoneOffset.UTC);

                if (days.containsKey(dayRounded)) {
                    days.put(dayRounded, hour);
                } else {
                    days.put(dayRounded, hour);
                }
            }
            hours.clear();
        }
        days.clear();
        return aggregateData;
    }

    private Map<Long, Double> getAggregateData(int detail, List<History> historyList, long from, long to) {
        if (historyList.isEmpty()) {
            return Collections.EMPTY_MAP;
        }
        Map<Long, Double> aggregateData = getAggregateLastData(detail, historyList);

        return aggregateData.entrySet().stream()
                .filter(value -> value.getKey() >= from && value.getKey() <= to)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private static Map<Long, Double> getAggregateLastData(int type, List<History> historyList) {
        switch (type) {
            case 4: {
                return historyList.stream()
                        .filter(time -> time.getDay() != null)
                        .collect(Collectors.toMap(key -> key.getDay().getTime(), value -> value.getDay().getValue(),
                                (o1, o2) -> {
                                    logger.error("Duplicate keys in getAggregateLastData type = {}: {}", type, o1);
                                    return o2;
                                }));
            }
            case 3: {
                return historyList.stream()
                        .filter(time -> time.getHoursList() != null)
                        .flatMap(e -> e.getHoursList().entrySet().stream())
                        .collect(Collectors.toMap(Map.Entry::getKey, value -> value.getValue().getValue(),
                                (o1, o2) -> {
                                    logger.error("Duplicate keys in getAggregateAvgData type = {}: {}", type, o1);
                                    return o2;
                                }));
            }
            case 2: {
                return historyList.stream()
                        .filter(time -> time.getMinutesList() != null)
                        .flatMap(e -> e.getMinutesList().entrySet().stream())
                        .collect(Collectors.toMap(Map.Entry::getKey, value -> value.getValue().getValue(),
                                (o1, o2) -> {
                                    logger.error("Duplicate keys in getAggregateAvgData type = {}: {}", type, o1);
                                    return o2;
                                }));
            }
            default: {
                return historyList.stream()
                        .filter(time -> time.getTimeList() != null)
                        .flatMap(history -> history.getTimeList().stream())
                        .collect(Collectors.toMap(Time::getTime, Time::getValue,
                                (o1, o2) -> {
                                    logger.error("Duplicate keys in getAggregateAvgData type = {}: {}", type, o1);
                                    return o2;
                                }));
            }
        }
    }

    /**
     * Исключаем задвоенные значения в случае изменения хэша документа (2 документа на один день, час).
     */
    private void excludeDoubleValuesInTimeSection(Map<Long, Double> timeValues, int detail) {
        ChronoUnit timeUnit;
        switch (detail) {
            case 4:
                timeUnit = ChronoUnit.DAYS;
                break;
            case 3:
                timeUnit = ChronoUnit.HOURS;
                break;
            case 2:
                timeUnit = ChronoUnit.MINUTES;
                break;
            default:
                return;
        }

        final Map<Long, Long> timeRoundedMap = new HashMap<>();
        final Set<Long> deleteTimeSet = new HashSet<>();
        for (Map.Entry<Long, Double> entry : timeValues.entrySet()) {
            LocalDateTime targetTime = LocalDateTime.ofEpochSecond(entry.getKey(), 0, ZoneOffset.UTC);
            long timeRounded = targetTime.truncatedTo(timeUnit).toEpochSecond(ZoneOffset.UTC);
            if (timeRoundedMap.containsKey(timeRounded)) {
                deleteTimeSet.add(timeRoundedMap.get(timeRounded));
            }
            timeRoundedMap.put(timeRounded, entry.getKey());
        }

        deleteTimeSet.forEach(timeValues::remove);
        timeRoundedMap.clear();
        deleteTimeSet.clear();
    }
}
