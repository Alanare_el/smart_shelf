package org.latysh.domain;

public final class Views {
    public interface Id {}

    public interface IdName extends Id {}

    public interface FullDevice extends Id {}
}
