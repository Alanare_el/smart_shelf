package org.latysh.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "devices")
public class Device {

    @Transient
    public static final String COLLECTION_NAME = "devices";

    @Id
    private String id;

    private String value;
    private String serial;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastActionTime;

    public Device(String value, LocalDateTime lastActionTime, String serial) {
        this.value = value;
        this.lastActionTime = lastActionTime;
        this.serial = serial;
    }

    public static String getCollectionName() {
        return COLLECTION_NAME;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public LocalDateTime getLastActionTime() {
        return lastActionTime;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public void setLastActionTime(LocalDateTime lastActionTime) {
        this.lastActionTime = lastActionTime;
    }

    public void setCreationDate(LocalDateTime now) {
    }
}
