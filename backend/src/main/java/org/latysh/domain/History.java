package org.latysh.domain;

import org.latysh.dto.Time;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Objects;

@Document(collection = "history")
public class History {

    private String id;
    private String deviceId;
    private String deviceSerial;
    private String currentValue;
    private String hash;
    private Time day;
    private LinkedList<Time> timeList;
    private HashMap<Long, Time> minutesList;
    private HashMap<Long, Time> hoursList;
    private HashMap<Long, Time> dayList;

    private String getDateFromInstant(Instant time) {
        String text = time.toString();
        return text.substring(0, 10);
    }


    public void setId() {
        Instant now = Instant.now();
        String date = getDateFromInstant(now);
        this.id = String.format("%s %s", date, deviceId);
    }

    public void setId(Instant time) {
        String date = getDateFromInstant(time);
        this.id = String.format("%s %s", date, deviceId);
    }

    public void setId(String time) {
        this.id = time;
    }

    public String getId() {
        return id;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceSerial() {
        return deviceSerial;
    }

    public void setDeviceSerial(String deviceSerial) {
        this.deviceSerial = deviceSerial;
    }

    public Time getDay() {
        return day;
    }

    public void setDay(Time day) {
        this.day = day;
    }

    public LinkedList<Time> getTimeList() {
        return timeList;
    }

    public void setTimeList(LinkedList<Time> timeList) {
        this.timeList = timeList;
    }

    public String getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(String currentValue) {
        this.currentValue = currentValue;
    }

    public HashMap<Long, Time> getMinutesList() {
        return minutesList;
    }

    public void setMinutesList(HashMap<Long, Time> minutesList) {
        this.minutesList = minutesList;
    }

    public HashMap<Long, Time> getHoursList() {
        return hoursList;
    }

    public void setHoursList(HashMap<Long, Time> hoursList) {
        this.hoursList = hoursList;
    }

    public HashMap<Long, Time> getDayList() {
        return dayList;
    }

    public void setDayList(HashMap<Long, Time> dayList) {
        this.dayList = dayList;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        History history = (History) o;
        return hash == history.hash &&
                Objects.equals(id, history.id) &&
                Objects.equals(deviceId, history.deviceId) &&
                Objects.equals(deviceSerial, history.deviceSerial) &&
                Objects.equals(minutesList, history.minutesList) &&
                Objects.equals(hoursList, history.hoursList) &&
                Objects.equals(dayList, history.dayList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deviceId, deviceSerial);
    }

    @Override
    public String toString() {
        return "History{" +
                "id='" + id + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", deviceSerial='" + deviceSerial + '\'' +
                ", hash=" + hash +
                ", minutesList=" + minutesList +
                ", hoursList=" + hoursList +
                ", dayList=" + dayList +
                '}';
    }
}
