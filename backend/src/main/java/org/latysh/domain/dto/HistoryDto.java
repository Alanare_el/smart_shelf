package org.latysh.domain.dto;

public class HistoryDto {
    private String historyId;
    private String deviceId;
    private String serial;

    /**/
    private String value;
    private String oldValue;
    private Long time;
    private Long oldTime;


    public String getHistoryId() {
        return historyId;
    }

    public void setHistoryId(String historyId) {
        this.historyId = historyId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Long getOldTime() {
        return oldTime;
    }

    public void setOldTime(Long oldTime) {
        this.oldTime = oldTime;
    }
}
