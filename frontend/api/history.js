import Vue from 'vue'

const history = Vue.resource('http://localhost:9000/history?deviceId=6b205cb9-eaef-440d-98ed-2765a718adf6&deviceSerial=123456&from=1567499697&to=1567599697&detail=1')

export default {
    get: history.get(),
}