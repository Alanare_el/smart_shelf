import Vue from 'vue'

const devices = Vue.resource('/device{/serial}')

export default {
    get: device => devices.get({serial: device.serial}),
    update: device => devices.update({id: device.id}, device),
    remove: id => devices.remove({id})
}