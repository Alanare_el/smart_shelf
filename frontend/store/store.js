import Vue from 'vue'
import Vuex from 'vuex'
import devicesApi from 'api/devices'
import historyApi from "api/history";

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        devices: frontendData.devices,
        profile: frontendData.profile,
        history: {}
    },
    getters: {
        sortedDevices: state => (state.devices || []).sort((a, b) => -(a.id - b.id)),
        historyData: state => state.history
    },
    mutations: {
        addDeviceMutation(state, device) {
            state.devices = [
                ...state.devices,
                device
            ]
        },
        updateDeviceMutation(state, device) {
            const updateIndex = state.devices.findIndex(item => item.id === device.id);

            state.devices = [
                ...state.devices.slice(0, updateIndex),
                device,
                ...state.devices.slice(updateIndex + 1)
            ]
        },
        removeDeviceMutation(state, device) {
            const deletionIndex = state.devices.findIndex(item => item.id === device.id);

            if (deletionIndex > -1) {
                state.devices = [
                    ...state.devices.slice(0, deletionIndex),
                    ...state.devices.slice(deletionIndex + 1)
                ]
            }
        },
        getHistoryByIntervalMutation(state, data) {
            state.history = data;

            console.log(state.history);
            console.log(Object.keys(state.history));
        }
    },
    actions: {
        async addDeviceAction({commit, state}, device) {
            const result = await devicesApi.get(device);
            const data = await result.json();
            const index = state.devices.findIndex(item => item.id === data.id);

            if (index > -1) {
                commit('updateDeviceMutation', data)
            } else {
                commit('addDeviceMutation', data)
            }
        },
        async updateDeviceAction({commit}, device) {
            const result = await devicesApi.update(device);
            const data = await result.json();
            commit('updateDeviceMutation', data)
        },
        async removeDeviceAction({commit}, device) {
            const result = await devicesApi.remove(device.id);

            if (result.ok) {
                commit('removeDeviceMutation', device)
            }
        },
        async getHistoryByIntervalAction({commit}) {
            const result = await historyApi.get;
            const data = await result.json();

            if (result.ok) {
                commit('getHistoryByIntervalMutation', data)
            }
        }
    }
})