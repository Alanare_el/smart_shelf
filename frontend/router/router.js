import Vue from 'vue'
import VueRouter from 'vue-router'
import DevicesList from 'pages/DeviceList.vue'
import Auth from 'pages/Auth.vue'
import Profile from 'pages/Profile.vue'
import Charts from "pages/Charts.vue";

Vue.use(VueRouter)

const routes = [
    {path: "/", component: DevicesList},
    {path: "/auth", component: Auth},
    {path: "/profile", component: Profile},
    {path: "/charts/:id/:serial", name: "charts", component: Charts, props: true}
]

export default new VueRouter({
    mode: 'history',
    routes
})